const puppeteer = require("puppeteer");
const fs = require("fs");
function delay(time) {
    return new Promise(function (resolve) {
        setTimeout(resolve, time);
    });
}

(async () => {
    const id = fs.readFileSync('users_submit.txt', 'utf8').split('\n');
    const name = [
        "Tran Anh",
        "Vu Ngoc",
        "Ngo Tra",
        "Luu Bich",
        "Nguyen Hong",
        "Vo Manh",
        "Nguyen Viet",
        "Nguyen Trong",
        "Nguyen Duc Quang",
        "Nguyen Van",
        "Bui Hoang",
        "Phan Minh",
        "Nguyen Duy Thanh",
        "Phan Thi Anh",
        "Phan Thi",
        "Nguyen Thi Hong",
        "Tran Thi Thu",
        "Vu Thi Hong",
        "Duong Thi Thuy",
        "Ha Thi",
        "Nguyen Giao",
        "Do Thu",
        "Tran Thi Thuy",
        "Nguyen Thi Ngoc",
        "Nguyen Bao",
        "Hoang Van",
        "Phan Anh",
        "Bui Thanh",
        "Nguyen Thi Hoang",
        "Le Hanh",
        "Nguyen Thi Thu",
        "Dang Thi",
        "Nguyen Do Hoang",
        "Pham Huy",
    ];
    const subName = [
        "Hong",
        "Linh",
        "My",
        "Ngoc",
        "Nhung",
        "Cuong",
        "Duong",
        "Duy",
        "Minh",
        "Phong",
        "Son",
        "Truong",
        "Vu",
        "Dao",
        "Duc",
        "Hanh",
        "Huyen",
        "Nhung",
        "Quynh",
        "Thuong",
        "Huong",
        "Huyen",
        "Linh",
        "Mai",
        "Ngoc",
        "Phu",
        "Tai",
        "Van",
        "Anh",
        "Duong",
        "Huong",
        "Lan",
        "Minh",
        "Nguyen",
    ];

    const phone = [
        "09092",
        "038467521",
        "039851298",
        "039106360",
        "038399445",
        "030266559",
        "039276124",
        "038777391",
        "038113968",
    ];
    for (let i = 0; i < id.length; i++) {
        const browser = await puppeteer.launch({ headless: true });
        try {
            let url =
                "https://inthecloud.withgoogle.com/cloud-learning-paths-22/register.html";
            const page = await browser.newPage();
            const navigationPromise = page.waitForNavigation();

            await page.goto(url);
            await navigationPromise;

            await delay(2000);
            await page.waitForSelector('input[name="FirstName"]');
            await page.click('input[name="FirstName"]');
            await page.type(
                'input[name="FirstName"]',
                name[Math.round(Math.random() * 30)],
                { delay: 15 }
            );
            await page.click('input[name="LastName"]');
            await page.type(
                'input[name="LastName"]',
                subName[Math.round(Math.random() * 30)],
                { delay: 15 }
            );
            await page.click('input[name="Company"]');
            await page.type(
                'input[name="Company"]',
                name[Math.round(Math.random() * 30)],
                { delay: 15 }
            );
            await page.click('input[name="Email"]');
            await page.type('input[name="Email"]', id[i], { delay: 15 });

            await page.evaluate(() => {
                document
                    .getElementsByClassName(
                        "mktoButton mdc-button mdc-button--unelevated"
                    )[0].click();
            });
            await delay(2000);
            // await page.click('input[name="Website"]');
            // await page.type(
            //     'input[name="Website"]',
            //     subName[Math.round(Math.random() * 30)] +
            //         Math.round(Math.random() * 30).toString() +
            //         ".com",
            //     { delay: 15 }
            // );
            await page.type("#Title", "Administrator");
            await page.click('input[name="Phone"]');
            await page.type(
                'input[name="Phone"]',
                "09092" + Math.round(Math.random() * 100000),
                { delay: 15 }
            );
            await page.select("#CountryCode", "VN");
            await page.select("#Rental_EMEA_15__c", "Getting-Started-lp");
            await delay(1000);
            await page.evaluate(() => {document.getElementById("mktoRadio_1198650_1").click();});
            await delay(2000);
            await page.evaluate(() => {
                document.getElementsByClassName("mktoButton mdc-button mdc-button--unelevated")[0].click();
            });
            await delay(3000);
            await console.log(id[i]);
            await browser.close();
        } catch (e) {
            await console.log(e);
            await console.log("'" + id[i] + "',");
            // await browser.close();
            continue;
            
        }
    }
    await console.log("done");
})();