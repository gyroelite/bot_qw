from qwiklabs.qwiklabs import Qwiklabs


with Qwiklabs(teardown=False) as bot :
    print("Select What Will You Do:")
    print("1. Register Qwiklabs Using Fakermail")
    print("2. Register Qwiklabs Using My Own Email")
    print("3. Login And Run Qwiklabs")
    print("4. Exit")
    operation = input("Enter Your Number: ")
    if operation == "1":
        bot.get_email1()
        bot.register_qwiklabs()
        bot.close_tab()
        bot.switch_tab_email()
        bot.qwiklabs_free()
        bot.close_tab()
        bot.switch_tab_email()
        bot.confirm_email()
    elif operation == "2":
        bot.get_email2()
        bot.register_qwiklabs()
        bot.qwiklabs_free()
        bot.quit_driver()
        bot.confirm_email()
    elif operation == "3":
        bot.login_run()
    elif operation == "4":
        print("Exit Bot, Bye!")
    else:
        bot.login_run()
