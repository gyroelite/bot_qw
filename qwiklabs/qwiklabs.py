import qwiklabs.constants as const
import qwiklabs.random_phone as rphone
import os
import pyperclip
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class Qwiklabs(webdriver.Chrome) :
    def __init__(self, driver_path=r"C:/bot_qw/driver", teardown=True):
        self.driver_path = driver_path
        self.teardown = teardown
        os.environ['PATH'] += self.driver_path
        super(Qwiklabs, self).__init__()
        self.minimize_window()
        self.implicitly_wait(15)

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.teardown:
            self.delete_all_cookies()
            self.quit()

    def get_email1(self):
    #Open Temp Mail
        print('Opening... Temp Mail')
        self.get(const.EMAIL_URL)
        time.sleep(3)
        self.email_window = self.current_window_handle
        print("Email Window Handle",self.email_window)

    #Copy Email
        mail = self.find_element(By.ID,'email-address')
        get_mail = mail.get_attribute('placeholder')
        pyperclip.copy(get_mail)
        print('Email Copied!')
        self.email = pyperclip.paste()
        print('Your email is' + ' ' + self.email)
        time.sleep(3)

    def get_email2(self):
    #Get Email
        get_mail = input('Enter Your Email: ')
        pyperclip.copy(get_mail)
        print('Email Saved To Clipboard!')
        self.email = pyperclip.paste()
        print('Your email is' + ' ' + self.email)
        time.sleep(1)

    def register_qwiklabs(self):
    #Open Qwiklabs in new tab
        self.switch_to.new_window()
        self.get(const.QW_URL)
        print('Qwiklabs Open')
        qw_window = self.current_window_handle
        print("Qw Window Handle",qw_window)
        time.sleep(3)
    #Fill form
        #first name
        fname_form = self.find_element(By.ID,'user_first_name')
        fname_form.send_keys(const.fname)
        #last name
        lname_form = self.find_element(By.ID,'user_last_name')
        lname_form.send_keys(const.lname)
        #company
        company_form = self.find_element(By.ID,'user_company_name')
        company_form.send_keys(const.company)
        #email
        email_form = self.find_element(By.ID,'user_email')
        email_form.send_keys(self.email)
        #password
        self.password = 'wecouldbe'
        print("Your password is ", self.password)
        password_form = self.find_element(By.ID,'user_password')
        password2_form = self.find_element(By.ID,'user_password_confirmation')
        password_form.send_keys(self.password)
        password2_form.send_keys(self.password)
        #wait for captcha solving
        WebDriverWait(self, 300).until(
            EC.text_to_be_present_in_element(
                (By.CSS_SELECTOR, 'a[href="/users/confirmation/new"]'),
                "Didn't receive confirmation instructions?"
            )
        )
        #submit
        #create_button = self.find_element(By.CSS_SELECTOR,'button[data-analytics-action="clicked_create_account"]')
        #create_button.click()

    def switch_tab_email(self):
        self.switch_to.window(self.email_window)
        #self.close()

    def close_tab(self):
        self.close()

    def qwiklabs_free(self):
        self.switch_to.new_window()
        self.get(const.FREE_URL)
        free_window = self.current_window_handle
        print("Qw Free Window Handle",free_window)
        time.sleep(3)
        #first name
        first_name = self.find_element(By.ID,'FirstName')
        first_name.click()
        first_name.send_keys(const.fname)
        #last name
        last_name = self.find_element(By.ID,'LastName')
        last_name.click()
        last_name.send_keys(const.lname)
        #business name
        business_name = self.find_element(By.ID,'Company')
        business_name.click()
        business_name.send_keys(const.company)
        #email
        email_name = self.find_element(By.ID,'Email')
        email_name.click()
        email_name.send_keys(self.email)
        #job title
        job_title = self.find_element(By.ID, 'Title')
        job_title.click()
        job_title.send_keys("Administrator")
        #job_name = self.find_element(By.CSS_SELECTOR,'option[value="Administrator"]')
        #job_name.click()
        #phone number
        phone_number = self.find_element(By.ID,'Phone')
        phone_number.click()
        phone_number.send_keys(rphone.output)
        #country
        country_name = self.find_element(By.CSS_SELECTOR,'option[value="AF"]')
        country_name.click()
        #learning path
        learn_path = self.find_element(By.CSS_SELECTOR,'option[value="Getting-Started-lp"]')
        learn_path.click()
        #yes checkbox
        yes_checkbox = self.find_element(By.CSS_SELECTOR,"input[value=\"Yes\"]")
        yes_checkbox.click()
        #submit click qw free
        submit_btn = self.find_element(By.CLASS_NAME,'mdc-button--unelevated')
        submit_btn.click()
        try:
            #website
            website_name = self.find_element(By.ID, 'Website')
            website_name.click()
            website_name.send_keys(const.website)
            submit_btn.click()
        except:
            pass
        time.sleep(2)

    def quit_driver(self):
        self.quit()

    def confirm_email(self):
        #wait for msg 1
        WebDriverWait(self, 480).until(
        EC.text_to_be_present_in_element(
            (By.CSS_SELECTOR, 'a[class="sender_email"]'),
            "null"
            )
        )
        #open message
        open_msg1 = self.find_element(By.XPATH, '//*[@id="mailbox"]/div/div/div[3]/a')
        open_msg1.click()
        # move to msg iframe
        WebDriverWait(self, 15).until(
            EC.frame_to_be_available_and_switch_to_it(
                (By.ID, 'myIframe')
            )
        )
        #click confirm email
        #confirm_email = self.find_element(By.XPATH, '//*[@id="mailbox"]/div[1]/div/div[3]/a')
        #confirm_email.click()
        #wait for the iframe
        WebDriverWait(self, 120).until(
            EC.frame_to_be_available_and_switch_to_it(
                (By.CSS_SELECTOR, 'iframe[class="l-ie-iframe-fix"]')
            )
        )
        WebDriverWait(self, 15).until(
            EC.text_to_be_present_in_element(
                (By.CSS_SELECTOR, 'p[class="alert__message"]'),
                'Your account was successfully confirmed. Please sign in.'
            )
        )
        self.close()
        self.switch_to.window(self.email_window)
        #back to email list
        back_email = self.find_element(By.XPATH,'/html/body/section[2]/div/div/div[2]/div[2]/div[1]/div/div[1]/a')
        back_email.click()
        #wair for msg 2
        WebDriverWait(self, 480).until(
        EC.text_to_be_present_in_element(
            (By.CSS_SELECTOR, 'a[class="sender_email"]'),
            "Google Cloud"
            )
        )
        WebDriverWait(self, 120).until(
            EC.frame_to_be_available_and_switch_to_it(
                (By.CSS_SELECTOR, 'iframe[class="l-ie-iframe-fix"]')
            )
        )
        time.sleep(5)

    def login_run(self):
        #Login Qwiklabs
        print('Opening... Qwiklabs')
        email_qw = input("Masukkan Email : ")
        pass_qw = "wecouldbe"
        self.get(const.LAB_URL3_2)
        time.sleep(1)
        #click login
        login_btn = self.find_element(By.CSS_SELECTOR, 'a[href="/users/sign_in"]')
        login_btn.click()
        #window handle
        self.lab_window = self.current_window_handle
        print("Lab Window Handle", self.lab_window)
        #fill form
        email_btn = self.find_element(By.ID, 'user_email')
        email_btn.send_keys(email_qw)
        pass_btn = self.find_element(By.ID, 'user_password')
        pass_btn.send_keys(pass_qw)
        sign_in = self.find_element(By.CSS_SELECTOR, 'button[data-analytics-action="clicked_sign_in"]')
        sign_in.click()
        time.sleep(3)
        #click start lab
        host = self.find_element(By.CLASS_NAME, "ql-lab-control-panel max-height control-panel js-lab-control-panel ql-lab-control-panel in-content-drawer")
        #self.execute_script(
        #    "return arguments[0].shadowRoot.getElementById('lab-content').shadowRoot.getElementById('lab-content-container').shadowRoot.getElementById('lab-content').shadowRoot.getElementById('lab-constructions').shadowRoot.getElementByClassName('js-lab-control-panel').shadowRoot.getElementById('lab-control-button').shadowRoot.getElementByTagName('ql-button').shadowRoot.getElementByTagName('button').click()",
        #    host)
        self.execute_script(
            "return arguments[0].shadowRoot.getElementById('lab-control-button').shadowRoot.getElementByTagName('ql-button').shadowRoot.getElemwntByClassName('mdc-button.mdc-button--unelevated.gm-button-fill.gm-text-label-button.ql-button.positive').click()",
            host)
        WebDriverWait(self, 120).until(
            EC.text_to_be_present_in_element(
                (By.XPATH, '//*[@id="identifierNext"]/div/'),
                "Next"
            )
        )
        #click next
        next = self.find_element(By.XPATH, '//*[@id="identifierNext"]/div/')
        next.click()
        WebDriverWait(self, 120).until(
            EC.visibility_of_element_located(
                (By.NAME, 'password')
            )
        )
        #fill pass
        pass_log = pyperclip(paste)
        pass_form = self.find_element(By.XPATH, '//*[@id="password"]/div[1]/div/div[1]/input')
        pass_form.send_keys(pass_log)
        #click accept
        accept = self.find_element(By.NAME, 'accept')
        accept.click()
        #agree
        agree = self.find_element(By.ID, 'mat-checkbox-2')
        agree.click()
        continu = self.find_element(By.XPATH, '//*[@id="mat-dialog-0"]/xap-deferred-loader-outlet/ng-component/mat-dialog-actions/cfc-progress-button/div[1]/button')
        continu.click()
        time.sleep(5)