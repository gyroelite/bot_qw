const puppeteer = require("puppeteer");
const fs = require("fs");
function delay(time) {
    return new Promise(function (resolve) {
        setTimeout(resolve, time);
    });
}

(async () => {
    const id = fs.readFileSync('users_start.txt', 'utf8').split('\n');
    const pass = [
        "wecouldbe",
    ];

    for (let i = 0; i < id.length; i++) {
        const browser = await puppeteer.launch({ headless: false });
        try {
            let url =
                "https://www.cloudskillsboost.google/focuses/33568?parent=catalog";   
            const page = await browser.newPage();
            const navigationPromise = page.waitForNavigation();
            await page.goto(url);
            await navigationPromise;
            await delay(2000);
            // Click Sign In
            await page.waitForSelector('a[href="/users/sign_in"]');
            await page.click('a[href="/users/sign_in"]');
            await navigationPromise;
            await delay(2000);
            // Input Account Details
            await page.click('#user_email');
            await page.type(
                '#user_email', id[i],
                { delay: 15 }
            );
            await page.click('#user_password');
            await page.type(
                '#user_password',pass[0],
                { delay: 15 }
            );
            // Click Login
            await page.click('button[data-analytics-action="clicked_sign_in"]');
            await delay(3000);
            await console.log(id[i]);
        } catch (e) {
            await console.log(e);
            await console.log("'" + id[i] + "',");
            // await browser.close();
            continue;
            
        }
    }
    await console.log("Qw Is Open, It's Time To Start!");
})();